package com.skillbranch.bestshop.utils;

public interface ConstantManager {
    String TAG_LOG = "BEST_SHOP";

    String AUTH_TOKEN = "AUTH_TOKEN";
    String PATTERN_EMAIL = "^[a-zA-Z_0-9]{3,}@[a-zA-Z_0-9.]{2,}\\.[a-zA-Z0-9]{2,}$";
    String PATTERN_PASSWORD = "^\\S{8,}$";
    String BASKET_COUNT = "BUSKET_COUNT";

    String BASE_URL = "https://skba1.mgbeta.ru/api/v1/";
    long MAX_CONNECTION_TIMEOUT = 5000;
    long MAX_READ_TIMEOUT = 5000;
    long MAX_WRITE_TIMEOUT = 5000;

    //    public static final int PICK_PHOTO_FROM_GALLERY = 111;
//    public static final int PICK_PHOTO_FROM_CAMERA = 272;
//    public static final int REQUEST_READ_EXTERNAL_STORAGE = 333;
    final int MANAGE_DOCUMENTS = 333;


    int PICK_PHOTO_FROM_GALLERY = 111;
    int PICK_PHOTO_FROM_CAMERA = 222;
    int REQUEST_READ_EXTERNAL_STORAGE = 333;


    String PHOTO_FILE_PREFIX = "IMG_";

    int REQUEST_PERMISSION_FOR_GALLERY_CODE = 2999;
    int REQUEST_PERMISSION_CAMERA_CODE = 3000;
    int REQUEST_PERMISSION_READ_EXTERNAL_STORAGE = 3001;

    int REQUEST_PROFILE_PHOTO_GALLERY = 1001;
    int REQUEST_PROFILE_PHOTO_CAMERA = 1002;
    String FILE_PROVIDER_AUTHORITY = "com.skillbranch.bestshop.fileprovider";
    String LAST_MODIFIED_HEADER = "Last-Modified";
    String IF_MODIFIED_SINCE_HEADER = "If-Modified-Since";


    String ADD_COMMENT_USERNAME = "";
    String ADD_COMMENT_AVATAR = "";

    int MIN_CONSUMER_COUNT = 1;
    int MAX_CONSUMER_COUNT = 3;
    int LOAD_FACTOR = 3;
    int KEEP_ALIVE = 120;
    long INITIAL_BACK_OFF_IN_MS = 1000;
    int UPDATE_DATA_INTERVAL = 30;
    int RETRY_REQUEST_COUNT = 5;
    int RETRY_REQUEST_BASE_DELAY = 500;
}

