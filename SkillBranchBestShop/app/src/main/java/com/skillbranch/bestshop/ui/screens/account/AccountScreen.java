package com.skillbranch.bestshop.ui.screens.account;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.skillbranch.bestshop.R;
import com.skillbranch.bestshop.data.rx.SimpleDisposableSubscriber;
import com.skillbranch.bestshop.data.storage.dto.ActivityPermissionsResultDto;
import com.skillbranch.bestshop.data.storage.dto.UserAddressDto;
import com.skillbranch.bestshop.data.storage.dto.UserInfoDto;
import com.skillbranch.bestshop.data.storage.dto.UserSettingsDto;
import com.skillbranch.bestshop.di.DaggerService;
import com.skillbranch.bestshop.di.scopes.AccountScope;
import com.skillbranch.bestshop.flow.AbstractScreen;
import com.skillbranch.bestshop.flow.Screen;
import com.skillbranch.bestshop.mvp.models.AccountModel;
import com.skillbranch.bestshop.mvp.presenters.AbstractPresenter;
import com.skillbranch.bestshop.mvp.presenters.IAccountPresenter;
import com.skillbranch.bestshop.mvp.presenters.MenuItemHolder;
import com.skillbranch.bestshop.mvp.presenters.RootPresenter;
import com.skillbranch.bestshop.ui.activities.RootActivity;
import com.skillbranch.bestshop.ui.screens.address.AddressScreen;
import com.skillbranch.bestshop.ui.screens.cart.CartScreen;

import dagger.Provides;
import flow.Flow;
import mortar.MortarScope;
import rx.Observable;
import rx.Subscription;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.skillbranch.bestshop.ui.screens.account.AccountView.EDIT_STATE;
import static com.skillbranch.bestshop.utils.ConstantManager.REQUEST_PERMISSION_CAMERA_CODE;
import static com.skillbranch.bestshop.utils.ConstantManager.REQUEST_PERMISSION_FOR_GALLERY_CODE;
import static com.skillbranch.bestshop.utils.ConstantManager.REQUEST_PROFILE_PHOTO_CAMERA;
import static com.skillbranch.bestshop.utils.ConstantManager.REQUEST_PROFILE_PHOTO_GALLERY;

@Screen(R.layout.screen_account)
public class AccountScreen extends AbstractScreen<RootActivity.RootComponent> {
    public static final String TAG = "AccountScreen";

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentComponent) {
        return DaggerAccountScreen_Component.builder()
                .rootComponent(parentComponent)
                .module(new Module())
                .build();
    }

    //region =========================== DI =====================
    @dagger.Module
    public class Module {
        @Provides
        @AccountScope
        AccountPresenter provideAccountPresenter() {
            return new AccountPresenter();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @AccountScope
    public interface Component {
        void inject(AccountPresenter accountPresenter);

        void inject(AccountView accountView);

        RootPresenter getRootPresenter();

        AccountModel getAccountModel();
    }
    //endregion

    //region =========================== Presenter =====================
    public class AccountPresenter extends AbstractPresenter<AccountView, AccountModel> implements IAccountPresenter {
        private final String TAG = AccountPresenter.class.getSimpleName();

        private int mCustomState = 1;

        public int getCustomState() {
            return mCustomState;
        }

        public void setCustomState(int mCustomState) {
            this.mCustomState = mCustomState;
        }

        private Subscription addressSub;
        private Subscription settingsSub;
        private String mPhotoFileString;
        private Subscription mUserInfoSub;

        private boolean mIsActivityResultWaiting;
        private boolean mIsActivityPermissionsResultWaiting;

        //region =========================== Lifecycle =====================
        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (getView() != null && getRootView() != null) {
                getView().initView();
            }

            subscribeOnAddressObs();
            subscribeOnSettingsObs();
            subscribeOnUserInfoObs();

            if (mIsActivityResultWaiting) {
                mRootPresenter.getActivityResultPublishUrlSubject()
                        .subscribe(new SimpleDisposableSubscriber<String>() {
                            @Override
                            public void onNext(String uri) {
                                Log.d(TAG, "AccountPresenter onNext avatarLocalUrl=" + uri);
                                mIsActivityResultWaiting = false;
                                mModel.saveProfileInfo(new UserInfoDto(getView().getUserName(),
                                                getView().getUserPhone(),
                                                uri));
                                getView().updateAvatarPhoto(Uri.parse(uri));
                            }
                        });
                Log.d(TAG, "AccountPresenter subscribed to mActivityResultPublishSubject");
            }

            if (mIsActivityPermissionsResultWaiting) {
                getPermissionsResultPublishSubject()
                        .subscribe(new SimpleDisposableSubscriber<ActivityPermissionsResultDto>() {
                            @Override
                            public void onNext(ActivityPermissionsResultDto result) {
                                mIsActivityPermissionsResultWaiting = false;
                                if (!result.isAllGranted()) {
                                    mRootPresenter.getRootView().showMessage("На операцию нет разрешений");
                                } else {
                                    switch (result.getRequestCode()) {
                                        case REQUEST_PERMISSION_CAMERA_CODE:
                                            takePhotoFromCamera();
                                            break;
                                        case REQUEST_PERMISSION_FOR_GALLERY_CODE:
                                            takePhotoFromGallery();
                                    }
                                }
                            }
                        });
            }
        }

        @Override
        protected void initActionBar() {
            View.OnClickListener listener = item -> {
                Flow.get(getView()).set(new CartScreen());
            };

            mRootPresenter.newActionBarBuilder()
                    .setTitle("Аккаунт")
                    .addAction(new MenuItemHolder("В корзину", R.layout.icon_count_busket, listener))
                    .build();
        }

        @Override
        protected void initDagger(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void onSave(Bundle outState) {
            super.onSave(outState);
            addressSub.unsubscribe();
            settingsSub.unsubscribe();
            mUserInfoSub.unsubscribe();
        }

        //endregion


        //region =========================== Subscription =====================
        private void subscribeOnAddressObs() {
            addressSub = subscribe(mModel.getAddressObs(), new ViewSubscriber<UserAddressDto>() {
                @Override
                public void onNext(UserAddressDto userAddressDto) {
                    if (getView() != null) {
                        getView().getAdapter().addItem(userAddressDto);
                    }
                }
            });
        }

        private void updateListView() {
            getView().getAdapter().reloadAdapter();
            subscribeOnAddressObs();
        }


        private void subscribeOnSettingsObs() {
            settingsSub = subscribe(mModel.getUserSettingsObs(), new ViewSubscriber<UserSettingsDto>() {
                @Override
                public void onNext(UserSettingsDto userSettingsDto) {
                    if (getView() != null) {
                        getView().initSettings(userSettingsDto);
                    }
                }
            });
        }

        private void subscribeOnUserInfoObs() {
            mUserInfoSub = subscribe(mModel.getUserInfoObs(), new ViewSubscriber<UserInfoDto>() {
                @Override
                public void onNext(UserInfoDto userInfoDto) {
                    if (getView() != null) {
                        getView().updateProfileInfo(userInfoDto);
                    }
                }
            });
        }

        //endregion

        //region ================= Permissions =================

        private Observable<ActivityPermissionsResultDto> getPermissionsResultPublishSubject() {
            return mRootPresenter.getActivityPermissionsResultSubject()
                    .filter(res ->
                            res.getRequestCode() == REQUEST_PERMISSION_CAMERA_CODE
                                    || res.getRequestCode() == REQUEST_PERMISSION_FOR_GALLERY_CODE);
        }

        @Override
        public void chooseCamera() {
            if (getRootView() != null) {
                String[] permissions = new String[]{CAMERA, WRITE_EXTERNAL_STORAGE};
                if (mRootPresenter.checkPermissions(permissions)) {
                    takePhotoFromCamera();
                } else {
                    if (mRootPresenter.requestPermissions(permissions, REQUEST_PERMISSION_CAMERA_CODE)) {
                        mIsActivityPermissionsResultWaiting = true;
                    } else {
                        getRootView().showMessage("Нет разрешений на выполнение данной операции");
                    }
                }
            }
        }

        private void takePhotoFromCamera() {
            Uri photoFileUri = mRootPresenter.createFileForPhoto();
            if (photoFileUri != null) {
                Intent takeCaptureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                takeCaptureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoFileUri);
                mRootPresenter.startActivityForResult(takeCaptureIntent, REQUEST_PROFILE_PHOTO_CAMERA);
                mIsActivityResultWaiting = true;
            }
        }

        @Override
        public void chooseGallery() {
            String[] permissions = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE};
            if (mRootPresenter.checkPermissions(permissions)) {
                takePhotoFromGallery();
            } else {
                if (mRootPresenter.requestPermissions(permissions, REQUEST_PERMISSION_FOR_GALLERY_CODE)) {
                    mIsActivityPermissionsResultWaiting = true;
                } else {
                    mRootPresenter.getRootView().showMessage("Нет разрешений на выполнение данной операции");
                }
            }
        }

        private void takePhotoFromGallery() {
            Intent intent = new Intent();
            intent.setType("image/*");
            if (Build.VERSION.SDK_INT < 19) {
                intent.setAction(Intent.ACTION_GET_CONTENT);
            } else {
                intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
            }
            mRootPresenter.startActivityForResult(intent, REQUEST_PROFILE_PHOTO_GALLERY);
            mIsActivityResultWaiting = true;
        }

        //endregion

        @Override
        public void clickOnAddress() {
            Flow.get(getView()).set(new AddressScreen(null));
        }

        @Override
        public void switchViewState() {
            Log.e(TAG, "switchViewState: " + getCustomState());
            if (getCustomState() == EDIT_STATE && getView() != null) {
                mModel.saveProfileInfo(getView().getUserProfileInfo());
            }
            if (getView() != null) {
                getView().changeState();
            }
        }

//        @Override
//        public void switchOrder(boolean isChecked) {
//            mAccountModel.saveOrderNotification(isChecked);
//        }
//
//        @Override
//        public void switchPromo(boolean isChecked) {
//            mAccountModel.savePromoNotification(isChecked);
//        }

        @Override
        public void takePhoto() {
            Log.e(TAG, "takePhoto: " + mCustomState + "EDIT STATE - " + EDIT_STATE);
            if (getView() != null) {
                if (mCustomState == EDIT_STATE) {
                    getView().showPhotoSourceDialog();
                }
            }
        }

        @Override
        public void removeAddress(int position) {
            mModel.removeAddress(mModel.getAddressFromPosition(position));
            updateListView();
        }

        @Override
        public void editAddress(int position) {
            Flow.get(getView()).set(new AddressScreen(mModel.getAddressFromPosition(position)));
        }


//        @Nullable
//        @Override
//        protected IRootView getRootView() {
//            return mRootPresenter.getView();
//        }

        public void switchSettings() {
            if (getView() != null) {
                mModel.saveSettings(getView().getSettings());
            }
        }
    }

    //endregion
}
