package com.skillbranch.bestshop.mvp.views;

import com.skillbranch.bestshop.data.storage.dto.ProductDto;

public interface IProductView extends IView {
    public void showProductView(ProductDto product);
    public void updateProductCountView(ProductDto product);
}
